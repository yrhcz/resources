# Contributing  

Want to contribute resources of your own? Open a pull request or an issue.

## Guidelines  
  * Ensure the content is actually useful
  * In order to prevent duplicate requests, please check existing Issues/Pull requests before creating your own
  * One suggestion per pull request
  * Pull requests and commits should have useful titles. 
  * Link formatting should resemble the existing document, ```[Summarization of Content](Link)```
  
Thanks!
