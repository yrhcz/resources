# Resource List

Educational or otherwise useful resources for becoming a better (web) developer / person.

## Table of Contents
  * Education
    * Blogs
  * Standards / Style Guides / Best Practices
    * Checklists
  * Useful Tools
    * Task Runners
    * CSS Pre-processors
    * Package Management
    * Project Scaffolding
    * Virtual Environments / Containers
    * Tunneling
  * Productivity
  * Other Repositories

### Education
  * [Codecademy](http://codecademy.com/)
  * [Bento](http://bento.io/)
  * [Egghead](http://egghead.io)
  * [Team Treehouse](https://teamtreehouse.com/)
  * [Libraries.io](https://libraries.io/)
  * [Hackr.io](http://hackr.io/)
  * [Learncode.academy](http://learncode.academy)
  * [Learncode.academy Career advice](https://www.youtube.com/watch?v=pB0WvcxTbCA)
  * [Staying up to Date](https://uptodate.frontendrescue.org)
  * Blogs
    * [CSS-Tricks](https://css-tricks.com/)
    * [HTML5 Rocks](http://www.html5rocks.com/en/)
    * [Treehouse Blog](http://blog.teamtreehouse.com/)
    * [CSS Wizardy](http://csswizardry.com)

### Standards / Style Guides / Best Practices
  * [CSS Wizardy Post on Page Performance](http://csswizardry.com/2013/01/front-end-performance-for-web-designers-and-front-end-developers/)
  * [Google Developer PageSpeed Insights](https://developers.google.com/speed/docs/insights/rules)
  * [Front-end Code Standards](https://isobar-idev.github.io/code-standards/)
  * [Stack Exchange Question on technical details that should be considered by web developers](https://programmers.stackexchange.com/questions/46716/what-technical-details-should-a-programmer-of-a-web-application-consider-before)
  * [Google Developer Web Fundamentals](https://developers.google.com/web/fundamentals/?hl=en)
  * Checklists
    * [Web Developer's Checklist](http://webdevchecklist.com/)
    * [Website Performance Checklist](http://websiteperformancechecklist.com/)

### Useful Tools
  * Task Runners
    * [Grunt](http://gruntjs.com/)
    * [Gulp](http://gulpjs.com/)
  * CSS Pre-processors
    * [Sass](http://sass-lang.com/)
    * [LESS](http://lesscss.org/)
  * Package Management
    * [Bower](http://bower.io)
  * Project Scaffolding
    * [Yeoman](http://yeoman.io)
  * Virtual Environments / Containers
    * [RVM](https://rvm.io/)
    * [Vagrant](https://www.vagrantup.com/)
    * [Docker](https://www.docker.com/)
  * Tunneling
    * [Ngrok](http://ngrok.com)

### Productivity
  * [Building an Impressive Tech Portfolio](http://skillcrush.com/2015/03/12/impressive-tech-portfolio/)
  * [Building Your Personal Brand](http://www.quicksprout.com/the-complete-guide-to-building-your-personal-brand/)
  * [Guide to Becoming a Freelance Developer](http://www.joyceakiko.com/ultimate-guide-2015/)
  * [Tips for Getting a Webdev Job](http://simplestepscode.com/how-to-get-a-web-development-job-in-four-steps/)
  * [40 hours of work in 16.7](http://www.chriswinfield.com/40-pomodoro-workweek/)
  * [Focus on your success](https://hitenism.com/focus-on-your-own-success/)
  * [IWT Guide to Making Money](http://www.iwillteachyoutoberich.com/ultimate-guide-to-making-money/)
  * [Networking Guide](http://www.bakadesuyo.com/2013/05/how-to-network/)
  * [Networking for Shy People](http://www.bakadesuyo.com/2013/05/networking-tips-for-shy-people/)
  * [Skyrocket Your Productivity](http://www.skyrocketyourproductivity.com/blog/)

### Other Repositories
  * [Book of Modern Front-end Tooling](https://github.com/tooling/book-of-modern-frontend-tooling)
  * [Free Programming Books](https://github.com/vhf/free-programming-books/blob/master/free-programming-books.md)
  * [Front-end Dev Bookmarks](https://github.com/dypsilon/frontend-dev-bookmarks)
  * [Best Practices and Style Guides](https://github.com/SalGnt/cscshttps://github.com/SalGnt/cscs)

## Contributing?  

Please see [CONTRIBUTING](https://github.com/portunes/resources/blob/master/CONTRIBUTING.md) for more information.




